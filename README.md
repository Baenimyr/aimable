# Aimable
Ce projet contient un ensemble simple de règles de configuration pour [ananicy](https://github.com/Nefelim4ag/Ananicy) ou [ananicy-cpp](https://gitlab.com/ananicy-cpp/ananicy-cpp).

Les programmes systèmes disposent d’une priorité supérieure à 0 pour offrir de la fluidité.
Les compilateurs et les tâches de fond se font le plus discrets possible.


## Installation
Copiez le dossier _ananicy.d_ dans _/etc_.

Ces fichiers de configuration ne devraient pas être en conflits avec d’autres projets comme [ananicy-rules](https://github.com/kuche1/minq-ananicy).